# -*- mode: python -*-
a = Analysis(['gui.py'],
             pathex=['/Users/gjoseph/Documents/Work/CEEO/IEL/sink'],
             hiddenimports=[],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
data = Tree('multitrack_player', prefix= 'multitrack_player', excludes= ['.git*'])
lib = Tree('lib', prefix= 'lib', excludes= ['.git*'])
align_lib = Tree('alignment/lib', prefix= 'alignment/lib', excludes= ['*.c,', 'Makefile'])
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='sink',
          debug=False,
          strip=None,
          upx=True,
          console=False )
coll = COLLECT(exe,
               data, lib, align_lib,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='sink')
app = BUNDLE(coll,
             name='sink.app',
             icon=None)
