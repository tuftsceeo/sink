from alignment import findTones as tones
from alignment import matchTimecodes as tcode
import ffaudIO

from string import Template
import json

import sys
import os
import subprocess
import math

version = "##version_number##"	# filled in from Git commit hash or tag by Makefile when packaged into standalone application (needs to be ##version_number##)

def alignDir(dirpath, progressCallback= None):
	if getattr(sys, 'frozen', False):
		# we are running in a PyInstaller bundle
		basedir = sys._MEIPASS
	else:
		# we are running in a normal Python environment
		basedir = os.path.dirname(__file__)

	os.chdir(basedir)
	ffaudIO.FFMPEG_CMD = 'lib/ffmpeg'	# use the bundled ffmpeg binary -- comment this out to use the installed version
	viewerFileName = os.path.join(dirpath, 'player.html')

	try:
		sources = sourcesInDir(dirpath)
		print "****** Found {} sources to align".format(len(sources))
		sources = align(sources, progressCallback= progressCallback)
		print "****** Rendering web viewer (it's {})".format(viewerFileName)
		renderWebViewer(sources, viewerFileName)
	except RuntimeError:
		print "Unalignable sources given"


class htmlTemplate(Template):
	delimiter = '%'

def sourcesInDir(dirpath):
	"""
	Build an array of sources for all media files in the given directory.
	Valid file extensions are .mov, .mp4, .wav

	Returns sources: [
		{
			'src': 'path to file',
			'name': 'user-assigned name',
			'type': 'video' or 'audio'
		},
		...
	]
	"""
	exts = {
		'mov': 'video',
		'mp4': 'video',
		'wav': 'audio'
	}
	try:
		files = os.listdir(dirpath)
	except(OSError):
		print "Error: %s not a valid OS directory" % dirpath
 		files = []
	validFiles = [ f for f in files if (f[0] != '.') and f.split('.')[-1].lower() in exts ]

	sources = [
		{
			'name': fname[ : fname.rfind('.')],
			'src': os.path.join(dirpath, fname),
			'type': exts[ fname.split('.')[-1].lower() ]
		}
		for fname in validFiles ]

	return sources

def align(sources, freq= 20000, noiseFreq= 19000, progressCallback= None):
	"""
	sources: [
		{
			'src': 'path to file',
			'name': 'user-assigned name',
			'type': 'video' or 'audio' # not actually needed here
		},
		...
	]
	Returns a new array of sources which contained valid sync tones, with these new fields filled in for each source:
		'syncs': [] (array of timecodes where sync pulses found)
		'in': inpoint of this source to play in sync with others
		'rate': rate at which to play

	Reads each path and finds the occurrences of tones of the specified frequency (defaults to 20 kHz), then
	calculates the offset of each source so its tones align with all the others.

	If given, progressCallback is called after each stage of the alignment process with arguments:
		float (0-1) 		: percentage complete
		string 				: message
		error (None, 1)		: error state

	Raises RuntimeError if there are not enough useful sources given to be aligned.
	"""

	##############################
	### Find tones in audio tracks

	# print '****** Analyzing media for alignment...'
	complete = 0
	n = float(len(sources)) * 2	# each source has 2 import steps: read file and find tones
	readWeight = 0.7			# arbitrarily say reading/finding tones takes 70% of time

	for source in sources:
		# print 'Reading {}...'.format(source['name'])
		if progressCallback:
			progressCallback( (complete/n) * readWeight, "Reading audio from {}...".format(os.path.basename(source['src'])), error=0)
		try:
			rate, data = ffaudIO.readAudio(source['src'], channels= 1)
		except IOError:
			# print 'Something is wrong with {}, skipping it'.format(source['name'])
			complete += 2
			if progressCallback:
				progressCallback( (complete/n) * readWeight, "Something is wrong with {}, skipping it".format(os.path.basname(source['src'])), error=0)
			source['syncs'] = []
			continue
		except OSError as e:
			if progressCallback:
				progressCallback( (complete/n) * readWeight, "OSError with {}: {}".format(os.path.basename(source['src']), e.strerror), error=0)
			source['syncs'] = []
			continue
		except:
			# print "Unexpected error: ", sys.exc_info()[0]
			complete += 2
			if progressCallback:
				progressCallback( (complete/n) * readWeight, "Unexpected error with {}: {}".format(os.path.basname(source['src']), sys.exc_info()[0] ), error=1)
			source['syncs'] = []
			continue

		complete += 1
		if progressCallback:
			progressCallback( (complete/n) * readWeight, "Looking for sync tones in {}...".format(os.path.basename(source['src'])), error=0)

		times = tones.toneSearch(data, rate, freq, noiseFreq, pulseLength= 1)
		complete += 1
		if progressCallback:
			progressCallback( (complete/n) * readWeight, 'Found {} tones in {}'.format(len(times), source['src']), error=0)

		source['syncs'] = times
		# print 'Found {} tones in {}: {}'.format(len(times), source['name'], times)

		# Handle the nametag mic's strange adpcm_ima_wav codec, which Chrome won't open
		# Convert WAV files of this codec into a normal one, overwriting the old file
		if len(times) > 2:	# only convert file if input was useful -- otherwise, don't waste time
			_, _, codecs = ffaudIO.fileInfo(source['src'])
			if codecs[0] == 'adpcm_ima_wav':
				# print "Converting {} to a more compatible format...".format(source['src'])
				ffaudIO.writeAudio(source['src'], data, rate)

	########################################
	### Calculate offsets between all tracks

	validSources = [s for s in sources if len(s['syncs']) > 2]	# maybe this could be 1? but 2 to be safe
	timecodes = [s['syncs'] for s in validSources]
	if len(timecodes) < 2:
		err = RuntimeError("Need 2 sources, each w/at least 2 detectable {} Hz tones, but {} given.".format(20000, len(timecodes)))
		if progressCallback:
			progressCallback( 1.0, err.message, error=5)
		raise err

	if progressCallback:
		progressCallback( readWeight, 'Aligning all sources...', error=0 )

	# print '****** Aligning tones ({})...'.format(', '.join( [s['name'] for s in validSources] ))
	offsets, rates, errors, fullTimeline = tcode.matchAllTimecodes(timecodes, 0.5)

	# Remove sources that couldn't be aligned
	try:
		validSources, offsets, rates, errors, fullTimeline = zip( *[x for x in zip(validSources, offsets, rates, errors, fullTimeline) if not math.isnan(x[1])] )
	except ValueError:
		err = RuntimeError("Sync tones do not align for any sources")
		if progressCallback:
			progressCallback( 1.0, err.message, error=5)
		raise err

	print validSources

	if progressCallback:
		progressCallback( 1.0, 'Aligned {} of {} sources'.format(len(validSources), len(sources)), error=0)

	for i, s in enumerate(validSources):
		s['in'] = offsets[i]
		s['rate'] = rates[i]

	format_string = '{:>15.10}' * 4
	print format_string.format('', 'Offset:', 'Rate:', 'Error:')
	for s, error in zip(validSources, errors):
		print format_string.format(s['name'], s['in'], s['rate'], error)

	return validSources

def renderWebViewer(sources, viewfileName):
	"""
	Fill in template for web viewer, write viewer, and open the viewer in a browser.
	"""

	subs = {}
	subs['sources'] = 'var sources = ' + json.dumps(sources)
	subs['version'] = version

	with open('multitrack_player/multitrack.js', 'r') as f:
		subs['multitrack'] = f.read()
	with open('multitrack_player/interface.js', 'r') as f:
		subs['interface'] = f.read()
	with open('multitrack_player/multitrack.css', 'r') as f:
		subs['css'] = f.read()
	with open('multitrack_player/includes/jquery-1.9.1.min.js', 'r') as f:
		subs['jquery'] = f.read()
	with open('multitrack_player/templates/multitrack.pytemplate', 'r') as f:
		template = htmlTemplate( f.read() )

	htmlOut = template.substitute(subs)

	with open(viewfileName,'w') as f:
		f.write( htmlOut )

	subprocess.call(['open', viewfileName ])


if __name__ == '__main__':
	print 'Version: {}'.format(version)
	if len(sys.argv) > 2:
		print "Useage: {} directory".format(sys.argv[0])
	elif len(sys.argv) == 1:
		dirpath = raw_input('Drag a directory here, then click on this window and press return: ').strip()
	else:
		dirpath = sys.argv[1]
	dirpath = dirpath.strip().replace('\\', '')
	if not os.path.isdir(dirpath):
		print "{} is not a directory!".format(dirpath)

	ffaudIO.FFMPEG_CMD = 'lib/ffmpeg'	# use the bundled ffmpeg binary -- comment this out to use the installed version
	viewerFileName = os.path.join(dirpath, 'player.html')

	def printer(percent, msg):
		print "{:.0%}: {}".format(percent, msg)

	sources = sourcesInDir(dirpath)
	print "****** Found {} sources to align".format(len(sources))
	sources = align(sources, progressCallback= printer)
	print "****** Rendering web viewer (it's {})".format(viewerFileName)
	renderWebViewer(sources, viewerFileName)
