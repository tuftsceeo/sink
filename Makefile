HASH := $(shell git log -1 --format="%h")
VERSION := $(shell git describe --abbrev=0 --tags)

# Build a standalone binary into /dist of the alignment program,
# which packages all required files (including python, ffmpeg, and the web viewer template)
all: gui.py align.py sink.spec ffaudIO.py alignment/* multitrack_player/* lib/*
	mv align.py align.py.backup
	# Insert version info into the program so we can track where "in the wild" binaries
	# came from
	sed -E 's/##version_number##/${VERSION} (commit ${HASH})/' align.py.backup > align.py
	yes | pyinstaller sink.spec
	mv align.py.backup align.py

# This part doesn't work. Sed + make == really dumb
#match = exe = EXE(pyz,
#insert = data_tree = Tree('multitrack_player', prefix= 'multitrack_player', excludes=['.git*'])
#spec-inject: spec
#	sed -i -e "s/$(match)/$(insert)\"$$"\n$(match)\"$$"\n/g" sink.spec

# Careful using this, because the spec file needs to be manually modified
# to include the data files (web viewer, ffmpeg, etc.)
spec: gui.py align.py ffaudIO.py alignment multitrack_player
	pyi-makespec --windowed -n sink gui.py

clean:
	rm -r build dist
