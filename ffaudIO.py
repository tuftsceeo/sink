import subprocess as sp
import sys
import re
import numpy as np

"""
Pipe out of ffmpeg as a subprocess to decode audio from files. Heavily adapted from @Zulko's moviepy:
https://github.com/Zulko/moviepy/blob/master/moviepy/audio/io/readers.py
"""

FFMPEG_CMD = 'ffmpeg'	# command to invoke ffmpeg (just `ffmpeg` if it's in the shell's search path,
						# otherwise path to the binary)

def fileInfo(filename):
	"""
	Returns duration (sec), rate (Hz) of named file
	"""

	ffmpeg = sp.Popen([FFMPEG_CMD, '-i', filename],
		stdout= sp.PIPE,
		stderr= sp.PIPE)
	ffmpeg.stdout.readline()
	ffmpeg.terminate()

	info = ffmpeg.stderr.read().decode('utf8')
	lines = info.splitlines()

	if "No such file or directory" in lines[-1]:
		raise IOError("%s not found! Wrong path ?"%filename)
	elif "Invalid data found" in lines[-1]:
		raise IOError("{} contains invalid data.".format(filename))

	# get rate
	line = next(l for l in lines if 'Audio: ' in l)
	rate = re.search(r' (\d+) Hz', line).group(1)
	rate = int(rate)

	# get duration in seconds
	line = next(l for l in lines if 'Duration: ' in l)
	match = re.search(r" (\d\d):(\d\d):(\d\d.\d+)", line)
	hms = map(float, match.groups() )
	sec = hms[0] * 3600 + hms[1] * 60 + hms[2]

	# get codecs used
	c_lines = [l.split(':') for l in lines if '  Stream #' in l]
	codecparts = [ c_line[3] for c_line in c_lines ]
	codecs = [ re.search(r" (\w+) \(", c).group(1) for c in codecparts ]

	return sec, rate, codecs

def readAudio(filename, dtype= np.dtype('<i2'), channels= 2):
	sec, rate, codecs = fileInfo(filename)

	bytes = dtype.itemsize
	f_format = 's' + str(bytes * 8) + 'le'	# __ signed little-endian bytes
	acodec = 'pcm_s' + str(bytes * 8) + 'le'

	ffmpeg = sp.Popen([FFMPEG_CMD, '-i', filename,
		'-vn',					# ignore video
		'-f', f_format,			# input format
		'-acodec', acodec,		# audio codec format 
		'-ar', str(rate),		# rate (Hz)
		'-ac', str(channels),	# channles to use
		'-'],					# pipe to stdout
		stdout= sp.PIPE,
		stderr= sp.PIPE)

	nsamples = int(sec * rate * channels)
	# totalBytes = nsamples * bytes

	audio = np.fromfile(ffmpeg.stdout, dtype, nsamples)
	if channels > 1:
		audio.shape = ( len(audio) / channels, channels )
	return rate, audio

def writeAudio(filename, data, rate, codec= None):
	"""
	Write the given raw audio data to disk as an audio file
	If no codec specified, defaults to an uncompressed WAV file of same byte format as input
	"""

	bytes = data.dtype.itemsize
	f_format = 's' + str(bytes * 8) + 'le'	# [num bits] signed little-endian bits
	acodec = 'pcm_s' + str(bytes * 8) + 'le'

	if codec is None:
		codec = acodec

	ffmpeg = sp.Popen([FFMPEG_CMD,
		'-y',												# overwrite without asking
		'-f', f_format,										# input format
		'-acodec', acodec,									# audio codec format 
		'-ar', str(rate),									# rate (Hz)
		'-ac', str(data.shape[1] if data.ndim == 2 else 1),	# channles to use
		'-i', '-',											# pipe from stdin
		'-acodec', codec,									# output codec
		'-ar', str(rate),									# output rate == input rate
		filename],											# filename to write to
		stdin= sp.PIPE,
		stderr= sp.PIPE)

	data.tofile(ffmpeg.stdin)

if __name__ == '__main__':
	from alignment import findTones as tones

	if len(sys.argv) < 2 or len(sys.argv) > 4:
	    print 'Useage: {} [target_frequency] [reference_frequency] filename'.format(sys.argv[0])
	else:
	    filename = sys.argv[-1]
	    print 'Reading file...'
	    rate, data = readAudio(filename, channels= 1)
	    print '...done.'
	    targetFreq, refFreq = 20000, 19000
	    if len(sys.argv) > 2:
	    	targetFreq = int(sys.argv[1])
	    if len(sys.argv) > 3:
	    	refFreq = int(sys.argv[2])
	    times = tones.toneSearch(data, rate, targetFreq, refFreq, pulseLength= 1)
	    print '***************'
	    print 'Found {} tones:'.format(len(times))
	    print times