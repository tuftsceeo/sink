import wx, time, os, sys, threading
import wx.lib.agw.multidirdialog as MDD
from wx.lib.pubsub import setupkwargs
from wx.lib.pubsub import pub

import align

K_ENTER = 13
K_DELETE = 8
K_QUIT = 81
DEFAULT_COL_WIDTH = 480


class DropTarget(wx.DropTarget):
    '''custom class to instatiate a drop target for a textctrl'''
    def __init__(self, listCtrl, textCtrl, *args, **kwargs):
        super(DropTarget, self).__init__(*args, **kwargs)
        self.listCtrl = listCtrl
        self.textCtrl = textCtrl
        self.composite = wx.DataObjectComposite()
        self.textDropData = wx.TextDataObject()
        self.fileDropData = wx.FileDataObject()
        self.thunderbirdDropData = wx.CustomDataObject('text/x-moz-message')
        self.composite.Add(self.thunderbirdDropData)
        self.composite.Add(self.textDropData)
        self.composite.Add(self.fileDropData)
        self.SetDataObject(self.composite)

    def OnDrop(self, x, y):
        return True

    def OnData(self, x, y, result):
    	#self.textCtrl.style=(wx.TE_MULTILINE | wx.NO_BORDER | wx.TE_RICH2 | wx.TE_NOHIDESEL)
        self.GetData()
        formatType, formatId = self.GetReceivedFormatAndId()
        if formatId == 'text/x-moz-message':
            return self.OnThunderbirdDrop()
        elif formatType in (wx.DF_TEXT, wx.DF_UNICODETEXT):
            return self.OnTextDrop()
        elif formatType == wx.DF_FILENAME:
            return self.OnFileDrop()

    def GetReceivedFormatAndId(self):
        format = self.composite.GetReceivedFormat()
        formatType = format.GetType()
        try:
            formatId = format.GetId() # May throw exception on unknown formats
        except:
            formatId = None
        return formatType, formatId

    def OnThunderbirdDrop(self):
    	self.listCtrl.AddItem(self.thunderbirdDropData.GetData().decode('utf-16'))
        return wx.DragCopy

    def OnTextDrop(self):
    	self.listCtrl.AddItem(self.textDropData.GetText() + '\n')
        return wx.DragCopy

    def OnFileDrop(self):
    	# note -- need to check that it's a folder
        for filename in self.fileDropData.GetFilenames():
        	self.listCtrl.AddItem(filename)
        return wx.DragCopy





class listCtrlClass(wx.ListCtrl):
	'''custom listcontrol'''
	def __init__(self, parent, pos, size):
		style = (wx.LC_REPORT | wx.BORDER_SUNKEN | wx.LC_SINGLE_SEL)
		super(listCtrlClass, self).__init__(parent, pos=pos, size=size, style=style)
		self.InsertColumn(0, 'Directories')
		self.InsertColumn(1, 'Progress')
		self.SetColumnWidth(0, DEFAULT_COL_WIDTH)
		self.SetColumnWidth(1, 150)
		self.col_widths = {DEFAULT_COL_WIDTH} # hold max col widths for each row
		self.index = 0 # index of next dir to be added

	def AddItem(self, path):
		'''add item (string) to listctrl'''
		self.InsertStringItem(self.index, path)
		self.Select(self.index)
		max_col_width = len(path) * 10
		if max_col_width > self.GetColumnWidth(0):
			self.SetColumnWidth(0, len(path) * 10)

		self.col_widths.add(self.GetColumnWidth(0))
		self.index += 1

	def RemoveSelected(self):
		'''remove selected item from listctrl'''
		selected = self.GetFirstSelected()
		if selected != -1:
			max_col_width = len(self.GetItemText(selected)) * 10
			if max_col_width > DEFAULT_COL_WIDTH:
				try:
					self.col_widths.remove(len(self.GetItemText(selected)) * 10)
				except:
					print 'error: default column width removed from set'

			self.DeleteItem(selected)
			self.SetColumnWidth(0, max(self.col_widths))
			self.index -=  1
			if selected != 0: # select previous dir in list
				self.Select(selected - 1, True)
			elif self.index != 0: # if already selected first, select next dir
				self.Select(0, True)

	def InitProgressState(self):
		'''add column for progress in list'''
		self.SetColumnWidth(0, 330)
		self.SetColumnWidth(1, 150)

class windowClass(wx.Frame):
	'''main window frame'''
	def __init__(self, parent, title):
		style = (wx.SYSTEM_MENU | wx.CLOSE_BOX | wx.CAPTION | wx.MINIMIZE_BOX)
		super(windowClass, self).__init__(parent, style=style, title=title, size=(600, 340))
		self.SetAutoLayout(True)
		self.InitUI()


	def InitUI(self):
		'''initialize UI components'''
		# LABELS
		self.label1 = wx.StaticText(self, -1, label="Drag and drop/add folders:", pos=(52,14), style=wx.ALIGN_CENTRE_HORIZONTAL)
		self.versionlabel = wx.StaticText(self, -1, label="version " + align.version, pos=(340, 14), style=wx.ALIGN_CENTRE_HORIZONTAL)

		# BUTTONS
		self.b1 = wx.Button(self, -1, label="Add directory", pos=(50, 170), size=(130,100))
		self.b2 = wx.Button(self, -1, label="Remove directory", pos=(197, 170), size=(160,100))
		self.b3 = wx.Button(self, -1, label="Batch process", pos=(375, 170), size=(160,100))

		# MULT DIRECTORY LISTCTRL
		# HIDDEN TEXTCTRL FOR D-N-D
		self.textCtrl = wx.TextCtrl(self, pos=(60, 44), size=(473, 138),
					style=wx.TE_MULTILINE
						| wx.NO_BORDER
						| wx.TE_RICH2
						| wx.TE_NOHIDESEL)
		self.listctrl = listCtrlClass(self, pos=(56, 40), size=(485, 150))
		self.textCtrl.SetDropTarget(DropTarget(self.listctrl, self.textCtrl))
		self.textCtrl.Bind(wx.EVT_SET_FOCUS, self.onHiddenCtrlFocus)
		self.textCtrl.SetInsertionPointEnd()

		# BIND EVENTS
		self.Bind(wx.EVT_BUTTON, self.SelectDir, self.b1)
		self.Bind(wx.EVT_BUTTON, self.RemoveDir, self.b2)
		self.Bind(wx.EVT_BUTTON, self.ProcessDir, self.b3)
		self.Bind(wx.EVT_CHAR_HOOK, self.OnKeyDown)

		# GAUGE
		#self.label = wx.StaticText(self, -1, label="Progress:", pos=(52, 244), style=wx.ALIGN_CENTRE_HORIZONTAL)
		self.progressNotif = wx.StaticText(self, -1, label="", pos=(52, 244), size=(10,10), style=wx.ALIGN_CENTRE_HORIZONTAL)
		# ^need to make this guy actually visible to user
		self.progressBar = wx.Gauge(self, -1, 100, pos=(53, 265), size=(484, 20))

		# DISPLAY
		self.Centre()
		self.Show(True)

	def onHiddenCtrlFocus(self, evt):
		'''never reveal focus on hidden ctrls'''
		self.SetFocus()
		evt.Skip()

	def OnKeyDown(self, evt):
		'''handle keydown events'''
		keycode = evt.GetKeyCode()
		if keycode == K_ENTER:
			self.SelectDir(evt)
		if evt.CmdDown() and keycode == K_QUIT:
			sys.exit()
		else:
			evt.Skip()

	def SelectDir(self, evt):
		'''dialogbox to add directory to list control'''
		#dlg = wx.DirDialog(self, "Add directory for batch processing:",
		#					style=wx.DD_DEFAULT_STYLE
		#						| wx.DD_DIR_MUST_EXIST
		#						| wx.DD_CHANGE_DIR
		#						| wx.CLOSE_BOX
		#				  )
		dlg = MDD.MultiDirDialog(self, title="Add directories", defaultPath=os.getcwd(),
			agwStyle=MDD.DD_MULTIPLE|MDD.DD_DIR_MUST_EXIST)

		if dlg.ShowModal() == wx.ID_OK:
			paths = dlg.GetPaths()
			for path in paths:
				self.listctrl.AddItem(os.path.join("/Volumes", path))
		dlg.Destroy()

	def RemoveDir(self, evt):
		self.listctrl.RemoveSelected()

	def ProcessDir(self, evt):
		## needs to event handle/callback with Gabe's align module
		'''process directories in listctrl by calling align module'''
		dirs = [self.listctrl.GetItem(row,0).GetText() for row in range(self.listctrl.GetItemCount())]
		if dirs:
			self.listctrl.InitProgressState()
			self.b1.Disable()
			self.b2.Disable()
			self.b3.Disable()
			self.textCtrl.SetEditable(False)
			pub.subscribe(self.UpdateProgressBars, 'ProgressBars')
			AlignThread(dirs).start()
		else:
			wx.MessageBox("Please select at least one directory to process")

		# align code needs to call the following to update progress
		#pub.sendMessage('align.progress', data=progress_%, error=None|error_msg, dir=None|output dir)


	def UpdateProgressBars(self, data, error):
		'''errors are reported as [error_state (0 or 1), directory_num, err_msg]
		   note: progressNotif is always updated w/error[2] regardless of error
		'''
		self.progressNotif.SetLabel(error[2])
		# print message (Either error or progress) in a text ctrl
		if error[0] == 0: # update progress using 'data'
			self.progressBar.SetValue( data )
			if self.progressBar.GetValue() == 100:
				self.b1.Enable()
				self.b2.Enable()
				self.b3.Enable()
				self.textCtrl.SetEditable(True)
				# done msg dialog code here
				self.progressBar.SetValue(-1)
				# have this^^ happen right after msg instead of after sleep
		else:
			print "***** dir num" + str(error[1]) + "***** error" + error[2]
			self.b1.Enable()
			self.b2.Enable()
			self.b3.Enable()
			self.textCtrl.SetEditable(True)
			pass
			# report error here using 'error':
			# update listctrl row w/error msg and continue w/thread


class AlignThread(threading.Thread):
	def __init__(self, dirs):
		threading.Thread.__init__(self)
		self.dirs = dirs

	def run(self):
		for i, directory in enumerate(self.dirs):
			def progressBarUpdate(percent, msg, error):
				print "percent: {}, message: {}".format(percent, msg)
				pub.sendMessage('ProgressBars', data= ( (percent / len(self.dirs)) + float(i) / len(self.dirs) )*100, error= [error, i, msg])

			align.alignDir(directory, progressCallback= progressBarUpdate)
			# alignDummyTest(directory, progressCallback= progressBarUpdate)

def alignDummyTest(directory, progressCallback= None):
	import time
	steps = 5

	for i in range(steps):
		print "step {}".format(i)
		time.sleep(0.5)

		if progressCallback:
			progressCallback(i / (float(steps)-1), "step {} done".format(i))


if __name__ == '__main__':
	app = wx.App()

	windowClass(None, 'Align Video & Audio Files')
	app.MainLoop()
